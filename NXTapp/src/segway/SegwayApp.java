package segway;

import java.lang.String;

public class SegwayApp {
	
	/**
	 * Point of entry for NXT application.
	 * @throws InterruptedException
	 */
	public static void main(String [] args) throws InterruptedException {
		SegwayServer s = new SegwayServer();
		SegwayController c = new SegwayController(s);
		c.control();
	}

}
