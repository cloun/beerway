package segway;

import lejos.robotics.Gyroscope;

/**
 * Gyroscope on remote device. Calculates angular velocity based on last measurements.
 *
 */
class RemoteGyroscope implements Gyroscope {
	private double sensorAngleLastTime = 0;
	private float sensorAngleLast = 0;
	private float gyroRaw;
	
	/**
	 * Updates actual angle and calculates current angular velocity.
	 * @param usbAngle angle coming from usb stream.
	 */
	public void updateAngle(float usbAngle) {
		double curTime = System.currentTimeMillis();
		if(sensorAngleLastTime != 0) {
			gyroRaw = (float)((usbAngle - sensorAngleLast)*1000/(curTime-sensorAngleLastTime));
		}
		else {
			gyroRaw = 0;
		}
		sensorAngleLastTime = curTime;
		sensorAngleLast = usbAngle;
    }
	
	public float getAngularVelocity() {
		return gyroRaw;
	}
	
	public void recalibrateOffset(){}
	
}