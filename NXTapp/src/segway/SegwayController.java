package segway;

import lejos.nxt.LCD;
import lejos.nxt.MotorPort;
import lejos.nxt.NXTMotor;
import lejos.robotics.Gyroscope;
import lejos.robotics.navigation.SegowayPilot;

/**
 * Class performs moving control of Beerway.
 */
class SegwayController  {
	
	SegowayPilot pilot; 
	SegwayServer srv;
	
	/**
	 * Builds instance of LeJOS SegowayPilot
	 * @param gyro - a gyroscope instance
	 */
	private void makePilot(Gyroscope gyro) {
		NXTMotor left = new NXTMotor(MotorPort.B);
		NXTMotor right = new NXTMotor(MotorPort.C);
		pilot = new SegowayPilot(left, right, gyro, SegowayPilot.WHEEL_SIZE_NXT1, 14.3);
		pilot.setMoveDelay(1000);
		pilot.setTravelSpeed(20);
	}
	
	public SegwayController(SegwayServer srv) {
		this.srv = srv;
	}
	
	/**
	 * Executes last command server has received
	 */
	private void executeCommand(){
		int cmd = srv.pullLastCommand();
		switch (cmd) {
		case 2:
			pilot.rotate(10);
			break;
		case 3:
			pilot.forward();
			break;
		case 4:
			pilot.backward();
			break;
		case 5:
			pilot.stop();
			break;
		default:
			break;
		}
	}

	/**
	 * Waits until connection between phone and NXT has been established.
	 * Than creates SegowayPilot and starts execute commands coming from the client.
	 */
	public void control () {
		while (true) {
			try {
				Thread.sleep(1000);
				//waiting while connect
				if (srv.isConnected()) {
					//when connected create pilot instance
					makePilot(srv.getGyroscope());
					while(true) {
						//then every second execute command if any
						executeCommand();
						Thread.sleep(1000);
					}
				}
			}
			catch (Exception e) {
				LCD.drawString(e.toString(), 0, 1);
		  		try {
		  			Thread.sleep(1000);
		  		}
		  		catch (InterruptedException e1 ) {}
			}
		}
	}
}