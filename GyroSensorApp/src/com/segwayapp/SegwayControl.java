package com.segwayapp;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import lejos.pc.comm.NXTConnector;

import java.lang.Thread;

/**
 * Class for controlling segway. Sends sensor data and commands to NXT block. 
 * Handles NXT connection establishment. 
 */
class SegwayControl implements GyroDataTransmitter {

	private NXTConnector conn = new NXTConnector();
	private DataInputStream inDat;
	private DataOutputStream outDat;
	private AndroidGyroscope gyro;
	private boolean connected = false;

	public SegwayControl() {
		gyro = new AndroidGyroscope(this);
	}
	
	
	/**
	 * When gyroscope data changed, data are sent to segway.
	 * @param a Angle data to send
	 */
	@Override
	public void angleChanged(float a) {
		sendGyroData(a);
	}
	
	/**
	 * Sends data packets to the NXT. Accounts for the fact that it might be called from different threads. 
	 * @param header simple int header. Represents action should be performed on packet receive.
	 * 0 - do nothing
	 * 1 - angle data
	 * 2 - rotate segway
	 * 3 - move forward
	 * 4 - move back
	 * 5 - stop segway
	 * @param angle Angle data to send
	 */
	private synchronized boolean sendPacket(int header, float body) {
		if (!connected) {
			return false;
		}
		else {
			try {
				outDat.writeByte(header);
				if (header == 1) {
					outDat.writeFloat(body);
				}
				outDat.flush();
				return true;
			}
			catch (IOException e) {
				nxtDeviceDeattached();
				return false;
			}
		}
	}
	
	/**
	 * Returns true if application is connected to the NXT.
	 */
	public boolean isConnected() {
		return connected;
	}
	
	/**
	 * Sends a packet with gyroscope data to NXT block.
	 * @param angle Angle data to send
	 */
	private boolean sendGyroData(float angle) {
		return sendPacket(1, angle);
	}
	
	/**
	 * Sends command to rotate the segway.
	 */
	public boolean sendRotate() {
		return sendPacket(2, 0);
	}
	
	/**
	 * Sends command to move forward.
	 */
	public boolean sendForward() {
		return sendPacket(3, 0);
	}
	
	/**
	 * Sends command to move backward.
	 */
	public boolean sendBackward() {
		return sendPacket(4, 0);
	}

	/**
	 * Sends command to stop the segway's movement.
	 */
	public boolean sendStop() {
		return sendPacket(5, 0);
	}
		
	/**
	 * Hadnler called on NXT attached event. Creates data streams, enables gyro sensor 
	 * and informs the application.
	 */
	private void nxtDeviceAttached() {
		inDat = new DataInputStream(conn.getInputStream());
		outDat = new DataOutputStream(conn.getOutputStream());
		connected = true;
		AppInfo.informText("NXT connected");
		gyro.enable();
	}

	/**
	 * Hadnler called on NXT deattached event. Closes data streams, disables gyro sensor 
	 * and informs the application.
	 */
	private void nxtDeviceDeattached() {
		connected = false;
		AppInfo.informText("NXT disconnected");
		gyro.disable();
		try {
			inDat.close();
			outDat.close();
			conn.close();
		} catch (IOException e) {}
	}

	/**
	 * Tries to connect over USB. Timeout is 1 second. No exceptions are thrown.
	 */
	public void connect() {
		try { 
			for (int i =0; i<10; i++) {
				if (conn.connectTo("usb://")) {
					nxtDeviceAttached();
					return;
				}
				Thread.sleep(100);
			}
		}
		catch (InterruptedException e){}
	}
	
	/**
	 * Disconnects from NXT block.
	 */
	public void disconnect() {
		nxtDeviceDeattached();
	}
	
	/**
	 * Destructor closing connections on object deletion.
	 */
	@Override
	protected void finalize() throws Throwable {
		nxtDeviceDeattached();
		super.finalize();
	}

}
