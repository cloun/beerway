package com.segwayapp;

/**
 * Interface for Gyroscope data transmitter.
 *
 */
interface GyroDataTransmitter {
	/**
	 * Called when gyroscope measures new angle data.
	 * @param a Current angle measured by gyroscope sensor.
	 */
	public void angleChanged(float a);
}