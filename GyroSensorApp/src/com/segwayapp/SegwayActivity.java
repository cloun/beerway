package com.segwayapp;

import java.io.IOException;

import android.os.Bundle;
import android.widget.TextView;
import android.app.Activity;
import android.view.View;
import android.view.WindowManager;

/**
 * Android application which communicates with NXT segway. Simple HTTP server is hosted during
 * application runtime. Server allows remote control of segway.
 * @author Sergii Pylypenko
 *
 */
public class SegwayActivity extends Activity {

	private BeerwayServer server;
	private SegwayControl controller;
	
	/**
	 * Android application's point of entry.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AppContext.setContext(this);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.linear_layout);
		TextView mainText = (TextView) findViewById(R.id.main);
		mainText.setText("Nxt is deattached");
		controller = new SegwayControl();
		server = new BeerwayServer(controller);
	}
	
	/**
	 * Starts the web server which obtains commands for robot based on requested uri.
	 * @param view
	 */
	public void startWebServer(View view) {
		try {
			server.start();
			AppInfo.informToast("Web server started");
		} catch (IOException e) {
			AppInfo.informToast("Failed to start web server");
		}
	}
	
	/**
	 * Stops the web server.
	 * @param view
	 */
	public void stopWebServer(View view) {
		server.stop();
		AppInfo.informToast("Web server stopped");
	}
	
	/**
	 * Tries to initiate communication with NXT block over USB.
	 * @param view
	 */
	public void connectToNXT(View view) {
		controller.connect();
	 }
	
	/**
	 * Disconnects from NXT block.
	 * @param view
	 */
	public void disconnectFromNXT(View view) {
		controller.disconnect();
	 }
	
	/**
	 * Aplication close handler
	 */
	@Override
	protected void onStop() {
		server.stop();
		controller.disconnect();
		super.onStop();
	}
}
