package com.segwayapp;

import fi.iki.elonen.NanoHTTPD;

import java.io.IOException;

/**
 * HTTP server running on Android application. Used for remote control over local network.
 * @author Milan Fabian
 */
public class BeerwayServer extends NanoHTTPD {

    private final SegwayControl controller;

    public BeerwayServer(SegwayControl controller) {
        super(8080);
        this.controller = controller;
        Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
    }
    /**
     * Serves a HTTP response. Command for NXT block is obtained from requested uri.
     */
    @Override
    public Response serve(IHTTPSession session) {
        String uri = session.getUri();
        if (uri.endsWith(".jpg")) {
            return serveImage(uri);
        }
        if (uri.startsWith("/move")) {
            makeMove(uri);
        }
        return new Response(Response.Status.OK, MIME_HTML,
                "<html><head><title>Beerway control</title></head><body>"
                + "<h2>Beerway control</h2><table><tr>"
                + "<td><a href='/move/forward'><img src='/forward.jpg' alt='forward'/></a></div></td>"
                + "<td><a href='/move/rotate'><img src='/rotate.jpg' alt='rotate'/></a></td></tr>"
                + "<tr><td><a href='/move/back'><img src='/backward.jpg' alt='backward'/></a></td>"
                + "<td><a href='/move/stop'><img src='/stop.jpg' alt='stop'/></a></td></tr></table>"
                + "<p><small><i>Web interface for controlling Beerway"
                + "<br>Project for IA158 Real Time Systems, spring 2014"
                + "<br>Sergii Pylypenko, Milan Fabian, Martin Kuriplach, Milan Kostelnik</small></i></p>"
                + "</body></html>");
    }

    private void makeMove(String uri) {
        if (uri.equals("/move/back")) {
            controller.sendBackward();
        } else if (uri.equals("/move/forward")) {
            controller.sendForward();
        } else if (uri.equals("/move/rotate")) {
            controller.sendRotate();
        } else if (uri.equals("/move/stop")) {
            controller.sendStop();
        }
    }

    private Response serveImage(String uri) {
        try {
            return new Response(Response.Status.OK, "image/jpeg", AppContext.getContext().getAssets().open(uri.substring(1)));
        } catch (IOException ex) {
            return new Response(Response.Status.NOT_FOUND, "html/text", ex.toString());
        }
    }

}
