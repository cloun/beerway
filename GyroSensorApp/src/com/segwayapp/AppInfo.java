package com.segwayapp;

import android.widget.TextView;
import android.widget.Toast;

/**
 * AppInfo class for displaying text during runtime. 
 * It informs user about events happening during application runtime.  
 * @author Sergii Pylypenko
 *
 */
class AppInfo {
	
	/**
	 * Creates a text frame hovering over the application for a short duration.
	 * @param text text to display
	 */
    public static void informToast(final String text) {
		
    	AppContext.getContext().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(AppContext.getContext(), text, duration);
                toast.show();
            }
        });
    }
    
    /**
     * Displays a text on screen. Mainly used for status report.
     * @param text text to display
     */ 
    public static void informText(final String text) {

    	AppContext.getContext().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView mainText = (TextView) AppContext.getContext().findViewById(R.id.main);
                mainText.setText(text);
            }
        });
    }
}