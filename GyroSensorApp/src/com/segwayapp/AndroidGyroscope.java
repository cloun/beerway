package com.segwayapp;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

/**
 * Gyroscope which handles angle changes. 
 *
 */
class AndroidGyroscope implements SensorEventListener {
	
	private SensorManager sManager;
	private GyroDataTransmitter transmitter;
	
	/**
	 * Constructor for Gyroscope class. Registers GyroDataTransmitter and its manager.
	 * @param t Gyroscope data transmitter
	 */
	public AndroidGyroscope(GyroDataTransmitter t) {
		sManager =  (SensorManager)AppContext.getContext().getSystemService(Context.SENSOR_SERVICE);
		transmitter = t;
	}
	
	/**
	 * Enables listening of angle changed events.
	 */
	public void enable() {
		sManager.registerListener(this, sManager.getDefaultSensor(Sensor.TYPE_ORIENTATION), SensorManager.SENSOR_DELAY_FASTEST);
	}
	
	/**
	 * Disables listening of angle changed events.
	 */
	public void disable() {
		sManager.unregisterListener(this);
	}
	
	/**
	 * Called when sensor values changed.
	 */
	@Override
	public void onSensorChanged(SensorEvent event) {
		transmitter.angleChanged(event.values[2]);
	}
	
	/**
	 * Called when sensor accuracy is changed.
	 */
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// Do nothing
	}
}