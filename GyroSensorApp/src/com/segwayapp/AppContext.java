package com.segwayapp;

import android.content.Context;
import android.hardware.usb.UsbManager;

/**
 * Class holding application context.
 */
public class AppContext {

    private static SegwayActivity context;
    
    /**
     * Application context setter
     */
    public static void setContext(SegwayActivity ctx) {
    	context = ctx;
    }
    
    /**
     * Application context getter
     */
    public static SegwayActivity getContext() {
    	return context;
    }

    /**
     * Returns usb manager according to the application context.
     */
    public static UsbManager getUSBManager() {
        return (UsbManager) context.getSystemService(Context.USB_SERVICE);
    }
    
}
