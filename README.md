**Beerway** is gyro-stalilized robot-segway using Android phone with remote control.

**Example video**:
https://www.youtube.com/watch?v=T9TtKiDFx3Y

**Hardware used**:

1\) NXT brick

2\) 2x motors for movement;

3\) Samsung GT-N7000;

4\) USB-OTG cable;

5\) remote control device — web browser;

**Software used**:

LeJOS

Eclipse with Android and LeJOS  plugins

Android 4.4

adb over wifi

**Project source code structure**:

**Beerway/GyroSensorApp** — application running on the Android phone.
Performs like USB client. Executes gyro sensor data and web interface
commands transmission to the NXT device.

**Beerway/NXTapp** — application running on the NXT brick. Performs like USB
server. Executes gyro sensor data readings and commands received from
the phone.